using Microsoft.Extensions.DependencyInjection;
using netcore3_backend.Negocio.Territorio;

namespace netcore3_backend.Configs
{

    public static class NegocioModule
    {

        public static IServiceCollection AddNegocioModule(this IServiceCollection services)
        {
            services.AddTerritorioModule();
            return services;
        }

    }
}