create table comuna (
   id_comuna            int                 not null,
   id_provincia         int                 null,
   nombre               varchar(100)         not null,
   constraint pk_comuna primary key (id_comuna)
);

create table provincia (
   id_provincia         int                 not null,
   id_region            int                 null,
   nombre               varchar(100)         not null,
   constraint pk_provincia primary key (id_provincia)
);

create table region (
   id_region            int                 not null,
   nombre               varchar(100)         not null,
   constraint pk_region primary key (id_region)
);

create table perfil_usuario (
   id_perfil_usuario    int                 not null,
   nombre               varchar(100)         not null,
   constraint pk_perfil_usuario primary key (id_perfil_usuario)
);

create table persona (
   id_persona           bigint   identity    not null,
   id_comuna            int                 null,
   run                  int                 null,
   nombres              varchar(255)         not null,
   apellido_paterno      varchar(255)         not null,
   apellido_materno      varchar(255)         null,
   fecha_nacimiento      date                 null,
   telefono             varchar(20)          null,
   email                varchar(100)         not null,
   ruta_archivo_imagen_perfil varchar(255) null,
   constraint pk_persona primary key (id_persona)
);

create unique index idx_persona_email on persona (
email
);

create unique index idx_persona_run on persona (
run
);

create table usuario (
   id_persona           bigint                 not null,
   id_perfil_usuario    int                 not null,
   username             varchar(255)         not null,
   password             varchar(500)         null,
   habilitado           bit              not null,
   constraint pk_usuario primary key (id_persona)
);

create unique index idx_usuario_username on usuario (
username
);

alter table comuna
   add constraint fk_comuna_reference_provinci foreign key (id_provincia)
      references provincia (id_provincia);

alter table provincia
   add constraint fk_provinci_reference_region foreign key (id_region)
      references region (id_region);

alter table usuario
   add constraint fk_usuario_reference_persona foreign key (id_persona)
      references persona (id_persona);

alter table usuario
   add constraint fk_usuario_reference_perfil_u foreign key (id_perfil_usuario)
      references perfil_usuario (id_perfil_usuario);

alter table persona
   add constraint fk_persona_reference_comuna foreign key (id_comuna)
      references comuna (id_comuna);
