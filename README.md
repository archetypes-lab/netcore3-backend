# netcore3-backend

.Net Core 3 Api Backend Project

## Base Datos

docker-compose 
```
sqlserver2019: 
    restart: unless-stopped
    image: mcr.microsoft.com/mssql/server:2019-latest
    ports: 
        - 1433:1433 
    environment: 
        - "ACCEPT_EULA=Y" 
        - "SA_PASSWORD=Z3k3P4ss."
        - "MSSQL_PID=Express"
```

Crear Nueva Base Datos

```
docker exec -it <container_id|container_name> /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P <your_password>
1> create database <basedatos>
2> go
1> exit
```


- crear certificados de desarrollo
dotnet dev-certs https --trust

