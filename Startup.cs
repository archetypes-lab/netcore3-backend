using System;
using System.Data.SqlClient;
using LinqToDB.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using netcore3_backend.Configs;

namespace netcore3_backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            var connectionString = Configuration.GetConnectionString("AppDatabase");


            // LINQ2DB SETTINGS

            DataConnection.DefaultSettings = new AppLinq2DBSettings(connectionString);

            // MODULES

            services.AddNegocioModule();

            // HTTP SESSION

            services.AddDistributedRedisCache(options => {
                options.Configuration = Configuration.GetValue<string>("HttpSessionRedisStore:ConnectionString");
                options.InstanceName = Configuration.GetValue<string>("HttpSessionRedisStore:InstanceName");
            });

            services.AddSession(options => {
                var httpSessionMinutes = Configuration.GetValue<int>("HttpSession:SessionTimeOutMinutes");
                options.IdleTimeout = TimeSpan.FromMinutes(httpSessionMinutes);
            });
            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {

            var connectionString = Configuration.GetConnectionString("AppDatabase");

            // Evolve 
            var sqlServerCon = new SqlConnection(connectionString);
            try
            {
                var evolve = new Evolve.Evolve(sqlServerCon, msg => logger.LogInformation(msg))
                {
                    Locations = new[] { "db/migrations" },
                    IsEraseDisabled = true,
                };
                evolve.Migrate();
            }
            catch (Exception ex)
            {
                logger.LogCritical("Database migration failed.", ex);
                throw;
            }


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSession();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
