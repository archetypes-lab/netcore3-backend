using Microsoft.Extensions.DependencyInjection;
using netcore3_backend.Negocio.Territorio.Repositories;
using netcore3_backend.Negocio.Territorio.Services;

namespace netcore3_backend.Negocio.Territorio
{

    public static class TerritorioModule
    {

        public static IServiceCollection AddTerritorioModule(this IServiceCollection services)
        {

            // Repositories
            services.AddScoped<RegionRepository>();
            services.AddScoped<ComunaRepository>();

            // Services
            services.AddScoped<RegionService>();
            services.AddScoped<ComunaService>();

            return services;
        }
    }

}