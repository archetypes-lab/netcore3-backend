using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqToDB;
using netcore3_backend.Framework.Repositories;
using netcore3_backend.Negocio.Territorio.Entities;

namespace netcore3_backend.Negocio.Territorio.Repositories
{

    public class ComunaRepository : LinqToDBAsyncRepository<Comuna, int>
    {
        public override async Task<IList<Comuna>> FindAll()
        {
            return await QueryForList(async db =>
            {
                var query =
                    from c in db.Comuna
                    orderby c.Nombre ascending
                    select c;
                return await query.ToListAsync();
            });
        }

        public override async Task<Comuna> FindById(int id)
        {
            return await QueryForObject(async db =>
            {
                var query =
                    from c in db.Comuna
                    where c.IdComuna == id
                    select c;
                return await query.FirstOrDefaultAsync();
            });
        }

        public async Task<IList<Comuna>> FindByIdRegion(int idRegion)
        {
            return await QueryForList(async db =>
            {
                var query =
                    from c in db.Comuna
                    join p in db.Provincia on c.IdProvincia equals p.IdProvincia
                    join r in db.Region on p.IdRegion equals r.IdRegion
                    where r.IdRegion == idRegion
                    select c;
                return await query.ToListAsync();
            });
        }
    }

}
