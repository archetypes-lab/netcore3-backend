using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqToDB;
using netcore3_backend.Framework.Repositories;
using netcore3_backend.Negocio.Territorio.Entities;

namespace netcore3_backend.Negocio.Territorio.Repositories
{
    public class RegionRepository : LinqToDBAsyncRepository<Region, int>
    {
        public override async Task<IList<Region>> FindAll()
        {
            return await QueryForList(async db =>
            {
                var query =
                    from r in db.Region
                    select r;
                return await query.ToListAsync();
            });
        }

        public override async Task<Region> FindById(int id)
        {
            return await QueryForObject(async db =>
            {
                var query =
                    from r in db.Region
                    where r.IdRegion == id
                    select r;
                return await query.FirstOrDefaultAsync();
            });
        }
    }

}