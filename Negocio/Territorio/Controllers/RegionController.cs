using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using netcore3_backend.Negocio.Territorio.Entities;
using netcore3_backend.Negocio.Territorio.Services;

namespace netcore3_backend.Negocio.Territorio.Controllers
{


    [ApiController]
    [Route("regiones")]
    public class RegionController : ControllerBase
    {

        private readonly ILogger<RegionController> log;

        private readonly RegionService regionService;

        public RegionController(
            ILogger<RegionController> log,
            RegionService regionService)
        {
            this.log = log;
            this.regionService = regionService;
        }

        [HttpGet("todas")]
        public async Task<IList<Region>> TraerTodasRegiones()
        {
            return await this.regionService.FindAllRegions();
        }

    }

}