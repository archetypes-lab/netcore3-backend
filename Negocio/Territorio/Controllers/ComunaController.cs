using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using netcore3_backend.Negocio.Territorio.Entities;
using netcore3_backend.Negocio.Territorio.Services;

namespace netcore3_backend.Negocio.Territorio.Controllers
{

    [ApiController]
    [Route("comunas")]
    public class ComunaController : ControllerBase
    {

        private readonly ILogger<ComunaController> log;

        private readonly ComunaService comunaService;

        public ComunaController(
            ILogger<ComunaController> log,
            ComunaService comunaService
        )
        {
            this.log = log;
            this.comunaService = comunaService;
        }

        [HttpGet()]
        public async Task<IList<Comuna>> TraerTodas()
        {
            log.LogDebug("-> TraerTodas.");
            return await this.comunaService.FindAll();
        }

        [HttpGet("{idComuna}")]
        public async Task<Comuna> TraerPorIdComuna(int idComuna)
        {
            log.LogDebug($"-> TraerPorIdComuna. idComuna: {idComuna}");
            return await this.comunaService.FindById(idComuna);
        }

        [HttpGet("id-region/{idRegion}")]
        public async Task<IList<Comuna>> TraerComunasPorIdRegion(int idRegion)
        {
            log.LogDebug($"-> TraerComunasPorIdRegion. idRegion: {idRegion}");
            return await this.comunaService.FindByIdRegion(idRegion);
        }

    }

}
