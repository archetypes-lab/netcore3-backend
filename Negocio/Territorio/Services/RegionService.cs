using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using netcore3_backend.Negocio.Territorio.Entities;
using netcore3_backend.Negocio.Territorio.Repositories;

namespace netcore3_backend.Negocio.Territorio.Services
{

    public class RegionService
    {

        private readonly RegionRepository regionRepository;
        private readonly ILogger log;

        public RegionService(RegionRepository regionRepository, ILogger<RegionService> log)
        {
            this.regionRepository = regionRepository;
            this.log = log;
        }

        public async Task<IList<Region>> FindAllRegions()
        {
            log.LogDebug(" RegionBusiness.FindAllRegions!");
            return await regionRepository.FindAll();
        }

    }

}
