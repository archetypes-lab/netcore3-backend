using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using netcore3_backend.Negocio.Territorio.Entities;
using netcore3_backend.Negocio.Territorio.Repositories;

namespace netcore3_backend.Negocio.Territorio.Services
{

    public class ComunaService
    {

        private readonly ComunaRepository comunaRepository;

        private readonly ILogger<ComunaService> log;

        public ComunaService(ComunaRepository comunaRepository, ILogger<ComunaService> log)
        {
            this.comunaRepository = comunaRepository;
            this.log = log;
            this.log.LogDebug("!!Instanciando ComunaService!!");
        }

        public async Task<Comuna> FindById(int idComuna)
        {
            return await this.comunaRepository.FindById(idComuna);
        }

        public async Task<IList<Comuna>> FindAll()
        {
            return await this.comunaRepository.FindAll();
        }

        public async Task<IList<Comuna>> FindByIdRegion(int idRegion)
        {
            return await this.comunaRepository.FindByIdRegion(idRegion);
        }

    }

}