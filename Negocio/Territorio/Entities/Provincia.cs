using System.Collections.Generic;
using LinqToDB.Mapping;
using netcore3_backend.Framework.Entities;

namespace netcore3_backend.Negocio.Territorio.Entities
{

    [Table("provincia")]
    public class Provincia : IBaseEntity
    {

        [Column("id_provincia"), PrimaryKey, NotNull]
        public int IdProvincia { get; set; }

        [Column("id_region"), Nullable]
        public int? IdRegion { get; set; }

        [Column("nombre"), NotNull]
        public string Nombre { get; set; }

        [Association(ThisKey = "IdRegion", OtherKey = "IdRegion", CanBeNull = true, Relationship = Relationship.ManyToOne)]
        public Region Region { get; set; }

        [Association(ThisKey = "IdProvincia", OtherKey = "IdProvincia", CanBeNull = true, Relationship = Relationship.OneToMany)]
        public IEnumerable<Comuna> Comunas { get; set; }

    }

}
