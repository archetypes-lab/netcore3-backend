
using LinqToDB.Mapping;
using netcore3_backend.Framework.Entities;

namespace netcore3_backend.Negocio.Territorio.Entities
{

    [Table("comuna")]
    public class Comuna : IBaseEntity
    {

        [Column("id_comuna"), PrimaryKey, NotNull]
        public int IdComuna { get; set; }

        [Column("id_provincia"), Nullable]
        public int? IdProvincia { get; set; }

        [Column("nombre"), NotNull]
        public string Nombre { get; set; }

        [Association(ThisKey = "IdProvincia", OtherKey = "IdProvincia", CanBeNull = true, Relationship = Relationship.ManyToOne)]
        public Provincia Provincia { get; set; }

    }

}
