using System.Collections.Generic;
using LinqToDB.Mapping;
using netcore3_backend.Framework.Entities;

namespace netcore3_backend.Negocio.Territorio.Entities
{

    [Table("region")]
    public class Region : IBaseEntity
    {
        [Column("id_region"), PrimaryKey, NotNull]
        public int IdRegion { get; set; }

        [Column("nombre"), NotNull]
        public string Nombre { get; set; }

        [Association(ThisKey = "IdRegion", OtherKey = "IdRegion", CanBeNull = true, Relationship = Relationship.OneToMany)]
        public IEnumerable<Provincia> provincias { get; set; }

    }

}
