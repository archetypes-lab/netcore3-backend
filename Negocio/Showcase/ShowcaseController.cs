using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace netcore3_backend.Negocio.Showcase
{


    [ApiController]
    [Route("showcase")]
    public class ShowcaseController : ControllerBase
    {

        private static readonly string SESSION_KEY_VALUE = "session-key-value";

        private readonly ILogger<ShowcaseController> logger;

        public ShowcaseController(
            ILogger<ShowcaseController> logger
        )
        {
            this.logger = logger;
        }

        [HttpGet("set-session-value")]
        public void setSessionValue([FromQuery] string sessionValue)
        {
            logger.LogDebug($"setiando en sesión: {sessionValue}");
            HttpContext.Session.SetString(SESSION_KEY_VALUE, sessionValue);
        }

        [HttpGet("get-session-value")]
        public string getSessionValue()
        {
            logger.LogDebug("obteniendo desde sesión");
            return HttpContext.Session.GetString(SESSION_KEY_VALUE);
        }

    }


}
