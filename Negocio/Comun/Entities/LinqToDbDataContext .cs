using LinqToDB;
using netcore3_backend.Negocio.Territorio.Entities;

namespace netcore3_backend.Negocio.Comun.Entities
{
    public partial class LinqToDbDataContext : LinqToDB.Data.DataConnection
    {
        public ITable<Region> Region { get { return this.GetTable<Region>(); } }
        public ITable<Provincia> Provincia { get { return this.GetTable<Provincia>(); } }

        public ITable<Comuna> Comuna { get { return this.GetTable<Comuna>(); } }

        /*
		public ITable<PerfilUsuario> PerfilesUsuarios { get { return this.GetTable<PerfilUsuario>(); } }
		public ITable<Persona>       Personas         { get { return this.GetTable<Persona>(); } }
		public ITable<Usuario>       Usuarios         { get { return this.GetTable<Usuario>(); } }*/

        partial void InitMappingSchema()
        {
        }

        public LinqToDbDataContext()
        {
            InitDataContext();
            InitMappingSchema();
        }

        public LinqToDbDataContext(string configuration)
            : base(configuration)
        {
            InitDataContext();
            InitMappingSchema();
        }

        partial void InitDataContext();
        partial void InitMappingSchema();
    }
}