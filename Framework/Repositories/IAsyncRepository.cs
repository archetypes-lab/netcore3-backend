using System.Collections.Generic;
using System.Threading.Tasks;
using netcore3_backend.Framework.Entities;

namespace netcore3_backend.Framework.Repositories
{
    public interface IAsyncRepository<T, K> where T : IBaseEntity
    {
        Task<T> FindById(K id);

        Task<IList<T>> FindAll();

        Task<K> Insert(T entity);

        Task<int> Update(T entity);

        Task<int> Delete(T entity);
    }
}