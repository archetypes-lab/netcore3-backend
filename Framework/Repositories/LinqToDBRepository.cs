using LinqToDB;
using netcore3_backend.Configs;
using netcore3_backend.Framework.Entities;
using netcore3_backend.Negocio.Comun.Entities;
using System;
using System.Collections.Generic;

namespace netcore3_backend.Framework.Repositories
{
    public abstract class LinqToDBRepository<T, K> : IRepository<T, K> where T : IBaseEntity
    {
        private const string CONFIG_NAME = AppLinq2DBSettings.DEFAULT_CONFIGURATION;

        public abstract T FindById(K id);

        public abstract IList<T> FindAll();

        protected T QueryForObject(Func<LinqToDbDataContext, T> function)
        {
            using (var db = new LinqToDbDataContext(CONFIG_NAME))
            {
                return function.Invoke(db);
            }
        }

        protected IList<T> QueryForList(Func<LinqToDbDataContext, IList<T>> function)
        {
            using (var db = new LinqToDbDataContext(CONFIG_NAME))
            {
                return function.Invoke(db);
            }
        }

        public K Insert(T entity)
        {
            using (var db = new LinqToDbDataContext(CONFIG_NAME))
            {

                return (K)db.InsertWithIdentity<T>(entity);
            }
        }

        public int Delete(T entity)
        {
            using (var db = new LinqToDbDataContext(CONFIG_NAME))
            {
                return db.Delete<T>(entity);
            }
        }

        public int Update(T entity)
        {
            using (var db = new LinqToDbDataContext(CONFIG_NAME))
            {
                return db.Update<T>(entity);
            }
        }

    }

}