using LinqToDB;
using netcore3_backend.Configs;
using netcore3_backend.Framework.Entities;
using netcore3_backend.Negocio.Comun.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace netcore3_backend.Framework.Repositories
{
    public abstract class LinqToDBAsyncRepository<T, K> : IAsyncRepository<T, K> where T : IBaseEntity
    {
        private const string CONFIG_NAME = AppLinq2DBSettings.DEFAULT_CONFIGURATION;

        public abstract Task<T> FindById(K id);

        public abstract Task<IList<T>> FindAll();

        protected async Task<T> QueryForObject(Func<LinqToDbDataContext, Task<T>> function)
        {
            using (var db = new LinqToDbDataContext(CONFIG_NAME))
            {
                return await function.Invoke(db);
            }
        }

        protected async Task<IList<T>> QueryForList(Func<LinqToDbDataContext, Task<IList<T>>> function)
        {
            using (var db = new LinqToDbDataContext(CONFIG_NAME))
            {
                return await function.Invoke(db);
            }
        }

        public async Task<K> Insert(T entity)
        {
            using (var db = new LinqToDbDataContext(CONFIG_NAME))
            {
                return (K) await db.InsertWithIdentityAsync<T>(entity);
            }
        }

        public async Task<int> Delete(T entity)
        {
            using (var db = new LinqToDbDataContext(CONFIG_NAME))
            {
                return await db.DeleteAsync<T>(entity);
            }
        }

        public async Task<int> Update(T entity)
        {
            using (var db = new LinqToDbDataContext(CONFIG_NAME))
            {
                return await db.UpdateAsync<T>(entity);
            }
        }

    }

}
