using System.Collections.Generic;
using netcore3_backend.Framework.Entities;

namespace netcore3_backend.Framework.Repositories
{
    public interface IRepository<T, K> where T : IBaseEntity
    {
        T FindById(K id);

        IList<T> FindAll();

        K Insert(T entity);

        int Update(T entity);

        int Delete(T entity);
    }
}